ADAravis support files
======================

This is Aravis 0.8.20 compiled as a static library. These files can be reproduced with the following steps:

* Install dependencies.
    ```
    $ sudo yum install meson glib2-devel libusb-devel libxml2-devel
    ```
* Clone the aravis repository and check out the desired version.

    **NOTE**: version 0.8.21 and later requires meson >=0.56 which is not available in CentOS 7.
    ```
    $ git clone https://github.com/AravisProject/aravis.git
    $ cd aravis
    $ git checkout -b v0.8.20 0.8.20
    ```
* Configure the source. Build a static library and disable optional features that are not needed by the EPICS driver.
    ```
    $ meson -Ddefault_library=static -Dusb=enabled -Dviewer=disabled -Dintrospection=disabled -Dgst-plugin=disabled -Ddocumentation=disabled build
    ```
* Build and install into a staging directory.
    ```
    $ DESTDIR=$(pwd)/staging ninja -C build install
    ```
* The library and header files can now be extracted from `staging/usr/local`.


